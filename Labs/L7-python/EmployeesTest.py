import json

import re

import time

__author__ = 'cmarc'

import unittest
from selenium import webdriver


class MyEmployeeTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.employees = json.load(open("WebApp\employees.json"))
        self.driver.get("http://127.0.0.1:5000/employees")
        print(self.driver.page_source)

    def tearDown(self):
        self.driver.close()

    def test_all_employees_exists(self):
        self.driver.maximize_window()
        self.driver.find_element_by_class_name("valid").click()
        time.sleep(2)
        src = self.driver.page_source
        for e in self.employees:
            text_found = re.search(re.compile(e["name"]), src)
            self.assertNotEqual(text_found, None)
            text_found = re.search(re.compile(str(e["cnp"])), src)
            self.assertNotEqual(text_found, None)
            text_found = re.search(re.compile(e["function"]), src)
            self.assertNotEqual(text_found, None)
            text_found = re.search(re.compile(str(e["salary"])), src)
            self.assertNotEqual(text_found, None)

    def test_employee_do_not_exists(self):
        self.driver.maximize_window()
        self.driver.find_element_by_class_name("invalid").click()
        time.sleep(2)
        src = self.driver.page_source
        for e in self.employees:
            text_found = re.search(re.compile(e["name"]), src)
            self.assertEqual(text_found, None)
            text_found = re.search(re.compile(str(e["cnp"])), src)
            self.assertEqual(text_found, None)
            text_found = re.search(re.compile(e["function"]), src)
            self.assertEqual(text_found, None)
            text_found = re.search(re.compile(str(e["salary"])), src)
            self.assertEqual(text_found, None)


if __name__ == '__main__':
    unittest.main()
