import json

from flask import Flask
from flask import render_template
from flask import request
from flask import send_file
from flask_table import Table, Col

app = Flask(__name__, static_url_path='')


class ItemTable(Table):
    def sort_url(self, col_id, reverse=False):
        pass

    name = Col('Name')
    cnp = Col('cnp')
    function = Col('function')
    salary = Col('salary')


# Get some objects
class Item(object):
    def __init__(self, name, cnp, did_function, salary):
        self.name = name
        self.cnp = cnp
        self.function = did_function
        self.salary = salary


employees = {
    "employees": [
        {
            "name": "Casian",
            "cnp": 1961207015563,
            "function": "CONF",
            "salary": 123.56
        },
        {
            "name": "Marc",
            "cnp": 1951107015563,
            "function": "LECTURER",
            "salary": 1223.56
        },
        {
            "name": "Nicoale",
            "cnp": 1951107015563,
            "function": "TEACHER",
            "salary": 123.56
        }
    ]
}


def get_employees_sorted():
    employees_list = employees["employees"]
    for i in range(len(employees_list) - 1):
        for j in range(i + 1, len(employees_list)):
            if employees_list[i]["salary"] < employees_list[j]["salary"]:
                employees_list[i], employees_list[j] = employees_list[j], employees_list[i]
            elif employees_list[i]["salary"] == employees_list[j]["salary"] \
                    and str(employees_list[i]["cnp"])[1:8] < str(employees_list[j]["salary"])[1:8]:
                employees_list[i], employees_list[j] = employees_list[j], employees_list[i]
    return employees_list
with open("employees.json", "w") as f:
    f.write(json.dumps(get_employees_sorted(), indent=4))


@app.route('/employees')
def list_employees():
    print(request.__str__())
    print(request.args)
    if len(request.args) > 0:
        print(request.args.get("Data"))
        opt = request.args.get("Data")
        if opt == 'Invalid':
            print("InvalidData")
            items = list()
            # for empl in get_employees_sorted():
            #     items.append(Item(empl["name"], empl["cnp"], empl["function"], empl["salary"]))
            #
            # Or, equivalently, some dicts
            # Or, more likely, load items from your database with something like
            # items = ItemModel.query.all()

            # Populate the table
            table = ItemTable(items)
            return table.__html__()

        elif opt == 'Valid':
            print("ValidData")
            items = list()
            # for empl in get_employees_sorted():
            #     items.append(Item(empl["name"], empl["cnp"], empl["function"], empl["salary"]))
            #
            # Or, equivalently, some dicts
            # Or, more likely, load items from your database with something like
            # items = ItemModel.query.all()
            items = get_employees_sorted()
            # Populate the table
            table = ItemTable(items)
            return table.__html__()
    else:
            return """<form action="/employees"><input class="invalid" type="submit" name="Data" value="Invalid">
<input class="valid" type="submit" name="Data" value="Valid"></form>"""
    # import things
    items = list()
    # for empl in get_employees_sorted():
    #     items.append(Item(empl["name"], empl["cnp"], empl["function"], empl["salary"]))
    #
    # Or, equivalently, some dicts
    items = get_employees_sorted()

    # Or, more likely, load items from your database with something like
    # items = ItemModel.query.all()

    # Populate the table
    table = ItemTable(items)
    return table.__html__()


app.run()
