__author__ = 'cmarc'

import unittest
import WikiTest
import EmployeesTest
import os
import HTMLRunner

direct = os.getcwd()


class MyTestSuite(unittest.TestCase):

    @staticmethod
    def test_issue():
        smoke_test = unittest.TestSuite()
        smoke_test.addTests([
            # unittest.defaultTestLoader.loadTestsFromTestCase(WikiTest.MyWikiTestCase),
            unittest.defaultTestLoader.loadTestsFromTestCase(EmployeesTest.MyEmployeeTest),
        ])

        outfile = open(direct + "\Lab7Test.html", "w")

        runner1 = HTMLRunner.HTMLTestRunner(
            stream=outfile,
            title='Selenium Test Report',
            description='Lab7 Tests'
        )

        runner1.run(smoke_test)


if __name__ == '__main__':
    unittest.main()
